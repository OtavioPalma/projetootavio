package com.deitel.messenger.sockets.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.StringTokenizer;

import com.deitel.messenger.MessageListener;
import static com.deitel.messenger.sockets.SocketMessengerConstants.*;

public class MessageReceiver implements Runnable
{
   private BufferedReader input;
   private MessageListener messageListener;
   private boolean keepListening = true;
   
   public MessageReceiver( MessageListener listener, Socket clientSocket ) 
   {
      messageListener = listener;
      
      try 
      {
         clientSocket.setSoTimeout( 5000 );
         
         input = new BufferedReader( new InputStreamReader( 
            clientSocket.getInputStream() ) );
      }
      catch ( IOException ioException ) 
      {
         ioException.printStackTrace();
      }
   }
   
   public void run() 
   {    
      String message;
      
      while ( keepListening ) 
      {   
         try 
         {            
            message = input.readLine();
         }
         catch ( SocketTimeoutException socketTimeoutException ) 
         {
            continue;
         }
         catch ( IOException ioException ) 
         {
            ioException.printStackTrace();            
            break;
         }

         if ( message != null ) 
         {
             System.out.println("RECEIVE SERVER: " + message);
            StringTokenizer tokenizer = new StringTokenizer( 
               message, MESSAGE_SEPARATOR );

            if ( tokenizer.countTokens() == 3 )
            {
                String comando = tokenizer.nextToken();
                String pontos = tokenizer.nextToken();
                String cor = tokenizer.nextToken();
                String concat = pontos + MESSAGE_SEPARATOR + cor;
                
               messageListener.messageReceived(comando, concat);
            }
            else
            {
               if ( message.equalsIgnoreCase(
                  MESSAGE_SEPARATOR + DISCONNECT_STRING ) ) 
                  stopListening();
            }
         }
      }
      
      try
      {         
         input.close();
      }
      catch ( IOException ioException ) 
      {
         ioException.printStackTrace();     
      }
   }
   
   public void stopListening() 
   {
      keepListening = false;
   } 
}