package com.deitel.messenger.sockets.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import static com.deitel.messenger.sockets.SocketMessengerConstants.*;

public class MulticastSender implements Runnable
{   
   private byte[] messageBytes;
   
   public MulticastSender( byte[] bytes ) 
   { 
      messageBytes = bytes;
   } 

   public void run() 
   {
      try 
      {         
         DatagramSocket socket = 
            new DatagramSocket( MULTICAST_SENDING_PORT );
         
         InetAddress group = InetAddress.getByName( MULTICAST_ADDRESS );
         
         DatagramPacket packet = new DatagramPacket( messageBytes, 
            messageBytes.length, group, MULTICAST_LISTENING_PORT );
         
         socket.send( packet ); 
          socket.close(); 
          System.out.println("Packet enviado: " + packet.getLength());
      } 
      catch ( IOException ioException ) 
      { 
         ioException.printStackTrace();
      } 
   } 
}