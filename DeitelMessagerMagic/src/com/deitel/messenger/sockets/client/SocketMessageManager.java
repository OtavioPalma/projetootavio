package com.deitel.messenger.sockets.client;

import java.net.InetAddress;
import java.net.Socket;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import com.deitel.messenger.MessageListener;
import com.deitel.messenger.MessageManager;
import static com.deitel.messenger.sockets.SocketMessengerConstants.*;

public class SocketMessageManager implements MessageManager
{
   private Socket clientSocket;
   private String serverAddress;
   private PacketReceiver receiver;
   private boolean connected = false;
   private ExecutorService serverExecutor;
   
   public SocketMessageManager( String address )
   {
      serverAddress = address;
      serverExecutor = Executors.newCachedThreadPool();
   }
   
   public void connect( MessageListener listener ) 
   {
      if ( connected )
         return;

      try
      {
         clientSocket = new Socket( 
            InetAddress.getByName( serverAddress ), SERVER_PORT );

         receiver = new PacketReceiver( listener );
         serverExecutor.execute( receiver );
         connected = true;
      }
      catch ( IOException ioException ) 
      {
         ioException.printStackTrace();
      }
   }
   
   public void disconnect( MessageListener listener ) 
   {
      if ( !connected )
         return;
      
      try
      {     
         Runnable disconnecter = new MessageSender( clientSocket, "", 
            DISCONNECT_STRING );         
         Future disconnecting = serverExecutor.submit( disconnecter );         
         disconnecting.get(); 
         receiver.stopListening();
         clientSocket.close();
      } 
      catch ( ExecutionException exception ) 
      {
         exception.printStackTrace();
      }
      catch ( InterruptedException exception ) 
      {
         exception.printStackTrace();
      }
      catch ( IOException ioException ) 
      {
         ioException.printStackTrace();
      }
     
      connected = false;
   } 
   
   public void sendMessage( String from, String message )
   {
      if ( !connected )
         return;
      
      serverExecutor.execute( 
         new MessageSender( clientSocket, from, message) );
   }
} 