package com.deitel.messenger.sockets.client;

import java.io.IOException;
import java.util.Formatter;
import java.net.Socket;

import static com.deitel.messenger.sockets.SocketMessengerConstants.*;

public class MessageSender implements Runnable 
{
   private Socket clientSocket;
   private String messageToSend;

   public MessageSender( Socket socket, String userName, String message ) 
   {
      clientSocket = socket;
      
      messageToSend = userName + MESSAGE_SEPARATOR + message;
   } 
   
   public void run() 
   {
      try
      {     
         Formatter output =
            new Formatter( clientSocket.getOutputStream() );
         output.format( "%s\n", messageToSend ); 
         output.flush(); 
      } 
      catch ( IOException ioException ) 
      {
         ioException.printStackTrace();
      } 
   } 
} 