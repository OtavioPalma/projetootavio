package com.deitel.messenger.sockets.client;

import com.deitel.messenger.MessageManager;
import com.deitel.messenger.SwingControlDemo;

public class DeitelMessenger
{   
   public static void main( String args[] ) 
   {
      MessageManager messageManager;
      
      if ( args.length == 0 )
         messageManager = new SocketMessageManager( "localhost" );
      else
         messageManager = new SocketMessageManager( args[ 0 ] );  
      
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SwingControlDemo(messageManager).setVisible(true);
            }
        });
      
   } 
}