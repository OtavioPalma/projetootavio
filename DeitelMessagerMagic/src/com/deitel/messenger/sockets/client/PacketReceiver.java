package com.deitel.messenger.sockets.client;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.DatagramPacket;
import java.net.SocketTimeoutException;
import java.util.StringTokenizer;

import com.deitel.messenger.MessageListener;
import static com.deitel.messenger.sockets.SocketMessengerConstants.*;

public class PacketReceiver implements Runnable 
{
   private MessageListener messageListener;
   private MulticastSocket multicastSocket;
   private InetAddress multicastGroup;
   private boolean keepListening = true;
   
   public PacketReceiver( MessageListener listener ) 
   {
      messageListener = listener;
      
      try
      {
         multicastSocket = new MulticastSocket(
            MULTICAST_LISTENING_PORT );
         
         multicastGroup = InetAddress.getByName( MULTICAST_ADDRESS );
         
         multicastSocket.joinGroup( multicastGroup ); 
         
      }
      catch ( IOException ioException ) 
      {
         ioException.printStackTrace();
      }
   }
   
   public void run() 
   {          
      while ( keepListening ) 
      {
         byte[] buffer = new byte[ MESSAGE_SIZE ];

         DatagramPacket packet = new DatagramPacket( buffer, 
            MESSAGE_SIZE );

         try
         {            
            multicastSocket.receive( packet ); 
         }
         catch ( SocketTimeoutException socketTimeoutException ) 
         {
            continue;
         }
         catch ( IOException ioException ) 
         {
            ioException.printStackTrace();
            break;
         }
         
         String message = new String( packet.getData() );
         System.out.println("PacketReceiver: " + packet.getLength());

         message = message.trim();

         StringTokenizer tokenizer = new StringTokenizer( 
            message, MESSAGE_SEPARATOR );

         if ( tokenizer.countTokens() == 3 ) 
         {
            String comando = tokenizer.nextToken();
            String pontos = tokenizer.nextToken();
            String cor = tokenizer.nextToken();
            String concat = pontos + MESSAGE_SEPARATOR + cor;
            messageListener.messageReceived(comando, concat );
         } 
      } 

      try 
      {
         multicastSocket.leaveGroup( multicastGroup ); 
         multicastSocket.close();
      } 
      catch ( IOException ioException )
      { 
         ioException.printStackTrace();
      } 
   } 
   
   public void stopListening() 
   {
      keepListening = false;
   } 
} 