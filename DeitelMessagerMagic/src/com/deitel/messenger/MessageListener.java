package com.deitel.messenger;

public interface MessageListener 
{
   public void messageReceived( String from, String message );
}