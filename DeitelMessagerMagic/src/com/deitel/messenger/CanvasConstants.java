package com.deitel.messenger;

public interface CanvasConstants 
{
    public static final int NONE = 0;
    public static final int DESENHO_LIVRE = 1;
    public static final int APAGAR = 2;
    public static final int ESCREVER = 3;
    public static final String POINT_SEPARATOR = "<";
}
