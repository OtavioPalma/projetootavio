package com.deitel.messenger;

import java.awt.Color;
import java.awt.Point;
import java.util.List;

public interface CommandListener 
{
    public int getCommand();
    public int getSizeEraser();
    public Color getColor();
    public String getMessage();
    
    public List<Point> getArrayPoint();
    public void setClearArrayPoint();
    public boolean getArrayPointIsReady();
    public void setArrayPoint(List<Point> list);
    public void restoreColor();
}
